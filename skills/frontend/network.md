---

# Сеть


## Уровни навыка
### Уровень 1
протокол HTTP: 

* из чего состоит URL
* GET POST PUT DELETE
* коды ответов 1xx, 2xx, 3xx, 4xx, 5xx

на практике:

* умеет сделать запрос и обработать ответ через fetch / библиотеку
* знаком с REST API
* может проанализировать запрос-ответ в DevTools > Network

### Уровень 2
протокол HTTP: 

* основные заголовки
* механизм работы cookies
* HTTPS
* CORS
* HTTP/2 
* аутентификация
* кеширование

основы сетей:

* TCP / IP
* SSL / TLS
* DNS

на практике:

* знает разные способы передачи данных с клиента на сервер (fetch, XHR)
* умеет передать данные в разных форматах (formdata, blob)
* может попинговать сервер
* отправить запрос через cURL
* сделать простой HTTP-server

### Уровень 3
протокол HTTP:

* CSP

основы сетей:

* как работает интернет
* модель OSI, основные протоколы передачи данных
* пакетная передача данных, анализ пакетов (например, с помощью wireshark)\
на практике:

* что такое websockets
* техника long-polling
* server sent events
* понимает, что такое проксирующий сервер (например через nginx)
* понимание graphQL
* понимание web rtc

## Метод оценки

## Как прокачать
* [PWA Broadcast No.10 - Как работает интернет?](https://wiki.tcsbank.ru/pages/viewpage.action?pageId=839361749)
* <https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-1--net-31177> 
* <https://code.tutsplus.com/tutorials/http-the-protocol-every-web-developer-must-know-part-2--net-31155> 
* <https://learn.javascript.ru/network> 
* <https://frontendmasters.com/books/front-end-handbook/2019/#4.1>
* <https://frontendmasters.com/books/front-end-handbook/2019/#4.4> 
* <https://medium.com/@bjtimi.007/networking-101-for-frontend-developers-5ea70062ca01> 
* <https://htmlacademy.ru/blog/education/what/brauzer-google> 
* <https://learn.javascript.ru/long-polling> 
* <https://learn.javascript.ru/server-sent-events> 
* книги
  * Таненбаум Д. Уэзеролл - Компьютерные сети
  * Олифер, Олифер - Компьютерные сети. Принципы, технологии, протоколы
  * [High Performance Browser Networking](https://hpbn.co/)
