---

# Angular — DI

## Уровни навыка
### Уровень 1
* Может заинжектить сервис

### Уровень 2
* Понимает иерархичность дерева инжекторов
* Создает свои injection токены
* useClass, useValue, useFactory
* Знает об Optional DI-декоратор 

### Уровень 3
* Использует фабрики в Injection токенах
* Lifecycle сервисов
* useExisting, forwardRef
* Почему провайдить сервисы в модулях — плохая затея?
* Разница между providers модуля и компонента
* Знает, когда происходит инициализация сущности в зависимости от способа инжектирования
* Сервисы providedIn и в providers
* multi-провайдеры

### Уровень 4
* providedIn — как появился и зачем нужен
* Знает о Host DI-декоратор
* Инжектор lazy-модуля
* Знает о функции inject, контексты её использования
* Понимает разницу между providers и viewProviders 

### Уровень 5
* Знает о том, как получить любую DI-сущность (в том числе и с использованием модификаторов) в фабриках providers и фабриках токенов

## Метод оценки

## Как прокачать
* https://angular.io/guide/dependency-injection
* [Что можно положить в механизм Dependency Injection в Angular?](https://habr.com/ru/company/tinkoff/blog/516622/)
* [Возможности Angular DI, о которых почти ничего не сказано в документации](https://habr.com/ru/company/tinkoff/blog/523160/)
* [Используем DI в Angular по максимуму — концепция частных провайдеров](https://habr.com/ru/company/tinkoff/blog/507906/)
* [Глобальные объекты в Angular](https://habr.com/ru/company/tinkoff/blog/548510/)
