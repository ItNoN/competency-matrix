---

# Angular — База

## Уровни навыка
### Уровень 1
* Знает два вида форм, уверенно может использовать хотя бы один из них.
* Знает, что такое пайпы. Использует готовые пайпы.
* Знает о ng-content, ng-container.
* Базовая настройка роутинга, router-outlet.
* Lifecycle: ngOnInit, ngOnDestroy

### Уровень 2
* Может использовать как реактивные, так и темлейтные формы. Понимает их преимущества.
* Пишет свои пайпы.
* Routing: Guards, базовая настройка lazy-routing
* Lifecycle: знает ngOnChanges и может рассказать, что он идет перед ngOnInit и почему
* Интерсепторы
* Знает о существовании Санитайзера и безопасности контента

### Уровень 3
* Может сам написать структурную директиву уровня ngIf.
* Routing: Resolvers, настройка нескольких router-outlet в одном приложении (named router-outlets).
* Lifecycle: понимает все хуки и их работу.
* Знает о ng-packagr, понимает процесс подключения ангуляр библиотек к приложениями

### Уровень 4
* Понимает, из каких частей состоит Angular
* Правильно строит масштабируемые приложения с использованием архитектурных best practices
* Способен решать задачи уровня написания собственной платформы.
* Способен написать кастомную стратегию загрузки модулей.

### Уровень 5
* Понимает, из каких частей состоит Angular и как их подменять
* Понимает микросинтаксис структурных директив.

## Метод оценки

## Как прокачать
* https://angular.io/docs
* https://indepth.dev/

