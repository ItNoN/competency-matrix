---


# Angular — Tools


## Уровни навыка
### Уровень 1
* Знает о cli и его базовых возможностях: ng new, ng generate, build / serve / lint / test
* Бегло читает angular.json

### Уровень 2
* Подключение кастомных коллекций схематиков
* Может редактировать angular.json проекта

### Уровень 3
* Написание собственных схематиков *ИЛИ*
* Опыт написания билдеров

### Уровень 4
* Написание собственных схематиков *И*
* Опыт написания билдеров

### Уровень 5
* Понимание AST
* Написание миграций

## Метод оценки

## Как прокачать
* https://angular.io/cli
* [Как перестать бояться и создать свой Angular CLI Builder](https://habr.com/ru/company/tinkoff/blog/509810/)
* [Total Guide To Custom Angular Schematics](https://medium.com/@tomastrajan/total-guide-to-custom-angular-schematics-5c50cf90cdb4)
* [Schematics — An Introduction](https://blog.angular.io/schematics-an-introduction-dc1dfbc2a2b2)
* https://indepth.dev/

