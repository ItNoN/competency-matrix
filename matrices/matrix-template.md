---


| Навык                                        | Junior | Junior+ | Middle | Middle+ | Senior | Comments                         |
| -------------------------------------------- | :----: | :-----: | :----: | :-----: | :----: | -------------------------------- |
| Frontend                                                                                    |                                  |
| [Angular Base](./skills/frontend/angular-base.md)                       | - | - | - | - | - |                                  |
| [Angular ChangeDetection](./skills/frontend/angular-changedetection.md) | - | - | - | - | - |                                  |
| [Angular DI](./skills/frontend/angular-di.md)                           | - | - | - | - | - |                                  |
| [Angular Tools](./skills/frontend/testing.md)                           | - | - | - | - | - |                                  |
| [Браузерный рендер](./skills/frontend/browser-render.md)                | - | - | - | - | - |                                  |
| [DOM API](./skills/frontend/dom-api.md)                                 | - | - | - | - | - |                                  |
| [Верстка](./skills/frontend/html.md)                                    | - | - | - | - | - |                                  |
| [Инструменты](./skills/frontend/tools.md)                               | - | - | - | - | - |                                  |
| [Отладка](./skills/frontend/debugging.md)                               | - | - | - | - | - |                                  |
| [Сеть](./skills/frontend/network.md)                                    | - | - | - | - | - |                                  |
| [Производительность и оптимизация](./skills/frontend/performance.md)    | - | - | - | - | - |                                  |
| [Безопасность](./skills/frontend/security.md)                           | - | - | - | - | - |                                  |
| [Тестирование](./skills/frontend/testing.md)                            | - | - | - | - | - |                                  |
| [PWA](./skills/frontend/pwa.md)                                         | - | - | - | - | - |                                  |
| Shared                                                                                      |                                  |
| [JS Base](./skills/shared/js-base.md)                                   | - | - | - | - | - |                                  |
| [JS Typescript](./skills/shared/js-typescript.md)                       | - | - | - | - | - |                                  |
| [JS Асинхронность](./skills/shared/js-async.md)                         | - | - | - | - | - |                                  |
| [RxJS](./skills/shared/rxjs.md)                                         | - | - | - | - | - |                                  |
| [VCS](./skills/shared/vcs.md)                                           | - | - | - | - | - |                                  |
| [Рефакторинг](./skills/shared/refactoring.md)                           | - | - | - | - | - |                                  |
| [Мониторинг](./skills/shared/monitoring.md)                             | - | - | - | - | - |                                  |
| [Проектирование](./skills/shared/system-design.md)                      | - | - | - | - | - |                                  |
