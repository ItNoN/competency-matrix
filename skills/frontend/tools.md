---

# Инструменты

## Уровни навыка
### Уровень 1
Знает про существование (webpack, babel, typescript, eslint, jest, lint-stage, ng etc.) зачем эти инструменты нужны и какая у них зона ответственности. Может внести изменения в конфигурацию.

### Уровень 2
Может внедрить и подключить новые плагины для используемых инструментов или расширить конфигурацию. Разбирается в advance уровня конфигурациях тех инструментов которые используются на проекты.

### Уровень 3
Способен полностью сконфигурировать новый проект используя весь перечень необходимых инструментов. Улучшает текущий инструментарий и привносит улучшения в DX или перфомансе проекта. Может обновить инструменты на следующие мажорные обновления с починкой брекейн чейнджей.

### Уровень 4
Разрабатывает и поддерживает плагины и расширения к инструментам (например правила для eslint, плагины для babel или webpack, трансформы для typescript, билдер для angular cli) 

## Метод оценки

## Как прокачать
