---

# JS — асинхронность

## Уровни навыка
### Уровень 1
Ajax

Умеет использовать инструменты для работы с асинхронным кодом:

* Callback
* Promise
* Async/Await
* setTimeout(), setInterval()

Знает как устроен Event Loop:

* Main thread
* Call stack
* Job queue
* Microtasks
* Macrotasks

### Уровень 2
* Знает, из чего состоит цикл работы Event Loop.
* Знает, как в Event Loop обрабатываются задачи на рендеринг. Знает, что такое requestAnimationFrame.
* Отлично знает Promise API.
* Знает про особенность работы Event Loop в разных движках.
* Обработка ошибок в асинхронном коде.

### Уровень 3
* Знает, что такое WebWorkers, ServiceWorkers, SharedWorkers.
* Знает их отличительные особенности и целевые сценарии использования.
* Знает про ограничение этого API.
* Знает как утроен Async/Await внутри и причем тут промисы и генераторы.
* Блокирующие и неблокирующие I/O функции.

## Метод оценки

## Как прокачать
* [MDN Asynchronous JS](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous)
* [You don't know JS: async and performance(rus)](https://github.com/devSchacht/You-Dont-Know-JS/tree/master/async%20%26%20performance)
* [Введение в AJAX](https://learn.javascript.ru/ajax-intro)
* [Service worker cookbook](https://serviceworke.rs/)
* [Филипп Робертс: Что за чертовщина такая event loop?](https://www.youtube.com/watch?v=8aGhZQkoFbQ)
  * <http://latentflip.com/loupe/>
