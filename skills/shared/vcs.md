---

# VCS

[[_TOC_]]

## Уровни навыка
### Уровень 1
*Знание:*
-   Что такое git
-   Что такое commit, репозиторий
-   Базовое устройство git:
    -   working directory
    -   staging
    -   repository
-   Работа с ветками:
    -   Создание, удаление, переименование
    -   Как посмотреть список веток и переключаться между ними
-   Конфигурирование гита:
    -   gitignore
    -   git config 
-   Базовые команды:
    -   add
    -   commit
    -   push
    -   fetch
    -   merge
    -   pull
    -   stash
-   Что такое merge request (pull request)
-   Навигация по истории

*Использование: *Знает принятый в команде flow, может использовать его без подсказок

### Уровень 2
*Знание:*
-   Отмена изменений
    -   Revert
    -   Reset
    -   Checkout
    -   Clean
-   Изменение истории
    -   commit --amend
    -   rebase
    -   cherry-pick
-   git-worktree
-   Знает альтернативы используемому в команде flow (git workflow, tbd, etc), может рассказать их плюсы и минусы
-   Знает про conventional commits и другие общепринятые системы составления коммит-месседжей

*Использование: *Умеет использовать git для обеспечения максимальной выразительности истории

### Уровень 3
*Знание:*

-   Продвинутые возможности:
    -   bisect
    -   rerere
    -   LFS
    -   filter-branch
    -   BFG
    -   git-filter-repo
  
## Метод оценки

## Как прокачать
### Уровень 1
-   [Первые три раздела Pro Git от Scott Chacon и Ben Straub](https://git-scm.com/book/ru/v2)
-   [Atlassian Git Tutorial](https://www.atlassian.com/git)
-   [Скринкаст по Git](https://learn.javascript.ru/screencast/git#intro-starting-video)
-   [Understanding Git --- Data Model](https://medium.com/hackernoon/https-medium-com-zspajich-understanding-git-data-model-95eb16cc99f5)
-   [firstaidgit](http://firstaidgit.ru/) - Коллекция часто задаваемых вопросов по Git с возможностью поиска
-   [git - the simple guide](http://rogerdudler.github.io/git-guide/index.ru.html) - простое руководство по работе с git. Ничего сложного ;)
-   [Git How To](https://githowto.com/ru) - еще руководство
-   [Resources to learn Git](https://try.github.io/) - от гитхаба
-   [Полезные команды для работы с Git](https://htmlacademy.ru/blog/boost/tools/useful-commands-for-working-with-git) - статья на HtmlAcademy
-   [Регистрация на Гитхабе. Работа через консоль](https://htmlacademy.ru/blog/boost/tools/register-on-github-work-with-console) - статья на HtmlAcademy
-   [LearnGitBranching](http://learngitbranching.js.org/) -- туториал по ветвлению
-   [Pro Git](https://git-scm.com/book/ru/v2) - книга о Git

### Уровень 2
-   [Хорошие материалы по Git от Atlassian](https://www.atlassian.com/git)
-   [Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/)
-   [Demystifying Git internals](https://medium.com/@pawan_rawal/demystifying-git-internals-a004f0425a70)
-   [HEAD~ vs HEAD^ vs HEAD@{}](https://stackoverflow.com/questions/26785118/head-vs-head-vs-head-also-known-as-tilde-vs-caret-vs-at-sign/26785200)
-   [Useful tricks you might not know about Git stash](https://medium.freecodecamp.org/useful-tricks-you-might-not-know-about-git-stash-e8a9490f0a1a)
-   [Introduction to GitLab Flow](https://docs.gitlab.com/ee/topics/gitlab_flow.html)
-   [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/)
