---

# Безопасность

## Уровни навыка
### Уровень 1
* Знает, как предотвращать атаки
  * XSS
  * SQL-инъекции
* CORS
  * JSONP
* SOP
* CSP (базовое понимание)
* Отличает аутентификацию от авторизации (улыбка)
* API:
  * noopener/noreferrer
  * iframe sandbox
  * Механизмы защиты cookie
    * Secure, HttpOnly, Domain/Path, Expires, SameSite

### Уровень 2
* Имеет представление:
  * CSRF
  * DoS, DDoS
* Умеет работать с контролем доступа

### Уровень 3
* Глубокое понимание всех типов атак, описанных в OWASP top-10

## Метод оценки

## Как прокачать
* [Основы XSS](https://hackware.ru/?p=1174&PageSpeed=noscript)
* [XSS](https://portswigger.net/web-security/cross-site-scripting)
* [XSS Game](https://xss-game.appspot.com/level1)
* [CSRF](https://portswigger.net/web-security/csrf)
* [SOP](https://developer.mozilla.org/en/docs/Web/Security/Same-origin_policy)
* [CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)
* [Preflight request](https://developer.mozilla.org/en-US/docs/Glossary/Preflight_request)
* [HTTP OPTIONS method](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/OPTIONS)
* [Authoritative guide to CORS (Cross-Origin Resource Sharing) for REST APIs](https://www.moesif.com/blog/technical/cors/Authoritative-Guide-to-CORS-Cross-Origin-Resource-Sharing-for-REST-APIs/)
* [XMLHttpRequest: кросс-доменные запросы](https://learn.javascript.ru/xhr-crossdomain)
* [OWASP XSS Prevention Cheat Sheet](https://www.owasp.org/index.php/XSS_(Cross_Site_Scripting)_Prevention_Cheat_Sheet#XSS_Prevention_Rules)
* [Из джунов в мидлы: зачем фронтенд-разработчику информационная безопасность?](https://proglib.io/p/iz-dzhunov-v-midly-zachem-frontend-razrabotchiku-informacionnaya-bezopasnost-2021-01-29) 
* [Безопасность веб-приложений -- Олег Мохов --- OWASP top 10](https://www.youtube.com/watch?v=HqUgIV2FMD8)
* [Access control vulnerabilities and privilege escalation](https://portswigger.net/web-security/access-control)
