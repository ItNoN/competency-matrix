---


| Навык                                        | Junior | Junior+ | Middle | Middle+ | Senior | Comments                         |
| -------------------------------------------- | :----: | :-----: | :----: | :-----: | :----: | -------------------------------- |
| Frontend                                                                                    |                                  |
| [Angular Base](./skills/frontend/angular-base.md)                       | 1 | 2 | 3 | 3 | 4 |                                  |
| [Angular ChangeDetection](./skills/frontend/angular-changedetection.md) | 1 | 2 | 2 | 3 | 4 |                                  |
| [Angular DI](./skills/frontend/angular-di.md)                           | 1 | 2 | 2 | 3 | 4 |                                  |
| [Angular Tools](./skills/frontend/angular-tools.md)                     | 1 | 1 | 2 | 3 | 3 |                                  |
| [Браузерный рендер](./skills/frontend/browser-render.md)                | 1 | 1 | 2 | 2 | 3 |                                  |
| [DOM API](./skills/frontend/dom-api.md)                                 | 1 | 1 | 1 | 1 | 2 |                                  |
| [Верстка](./skills/frontend/html.md)                                    | 1 | 1 | 2 | 2 | 2 |                                  |
| [Инструменты](./skills/frontend/tools.md)                               | 1 | 1 | 2 | 2 | 3 |                                  |
| [Отладка](./skills/frontend/debugging.md)                               | 1 | 2 | 2 | 3 | 4 |                                  |
| [Сеть](./skills/frontend/network.md)                                    | 1 | 1 | 2 | 2 | 3 |                                  |
| [Производительность и оптимизация](./skills/frontend/performance.md)    | 0 | 1 | 1 | 2 | 3 |                                  |
| [Безопасность](./skills/frontend/security.md)                           | 0 | 1 | 1 | 2 | 2 |                                  |
| [Тестирование](./skills/frontend/testing.md)                            | 1 | 1 | 2 | 3 | 4 |                                  |
| [PWA](./skills/frontend/pwa.md)                                         | 1 | 1 | 1 | 2 | 2 |                                  |
| Shared                                                                                      |                                  |
| [JS Base](./skills/shared/js-base.md)                                   | 1 | 1 | 2 | 2 | 2 |                                  |
| [JS Typescript](./skills/shared/js-typescript.md)                       | 1 | 1 | 2 | 2 | 3 |                                  |
| [JS Асинхронность](./skills/shared/js-async.md)                         | 1 | 2 | 2 | 2 | 3 |                                  |
| [RxJS](./skills/shared/rxjs.md)                                         | 1 | 1 | 2 | 2 | 3 |                                  |
| [VCS](./skills/shared/vcs.md)                                           | 1 | 1 | 2 | 2 | 2 |                                  |
| [Рефакторинг](./skills/shared/refactoring.md)                           | 0 | 1 | 2 | 3 | 4 |                                  |
| [Инфраструктура](./skills/shared/infrastructure.md)                     | - | - | - | - | - |                                  |
| [Мониторинг](./skills/shared/monitoring.md)                             | 1 | 1 | 2 | 3 | 3 |                                  |
| [Проектирование](./skills/shared/system-design.md)                      | 1 | 1 | 2 | 3 | 4 |                                  |
| [Документирование](./skills/shared/documenting.md)                      | - | - | - | - | - |                                  |
