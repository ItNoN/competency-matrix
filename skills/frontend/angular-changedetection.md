---

# Angular — ChangeDetection

## Уровни навыка
### Уровень 1
* Знает о двух стратегиях проверки изменений

### Уровень 2
* Знает о существование zone.js
* Может объяснить поведение Change Detection стратегий
* Может орудовать ChangeDetectorRef: понимает разницу markForCheck и detectChanges

### Уровень 3
* Знает, как работает zone.js
* Понимание, как связан Angular и zone.js
* Знает о runOutsideAngular

### Уровень 4
* Знает, как частично отключить манки-патчинг zone.js 
* Создание локальных областей проверки изменений
* Может описать ChangeDetetion в ng-template

### Уровень 5
* Знает, почему в ангуляре не получится использовать нативный async/await

## Метод оценки

## Как прокачать
* https://angular.io/guide/zone
* https://angular.io/api/core/ChangeDetectorRef
* https://medium.com/angular-in-depth/everything-you-need-to-know-about-change-detection-in-angular-8006c51d206f
  * (перевод ее же: https://habr.com/ru/post/327004/)
* https://blog.mgechev.com/2017/11/11/faster-angular-applications-onpush-change-detection-immutable-part-1/
